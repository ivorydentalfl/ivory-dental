Ivory Dental is a full service dental office that combines complete professionalism with personal care and friendly communication. You can see from our reviews that our results  exceed the expectations of our patients.

Address: 11362 San Jose Blvd, #7, Jacksonville, FL 32223, USA

Phone: 904-998-1555
